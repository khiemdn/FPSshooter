﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UIManagerMulti.cs" company="VNG">
// 
// </copyright>
// <summary>
//   Defines the UIManagerMulti.cs
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using UnityEngine;
using UnityEngine.UI;

public class UIManagerMulti : MonoBehaviour
{
    public GameObject ConnectionGroupUI;

    public GameObject SideGroupUI;

    public Text connInfo;

    public static UIManagerMulti Instance
    {
        get
        {
            return _instance;
        }
    }

    private static UIManagerMulti _instance;

    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }
        if (_instance != this)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(this);
        this.ConnectionGroupUI.SetActive(true);
        this.SideGroupUI.SetActive(false);
        connInfo.text = "";
    }

    public void Connected(string info)
    {
        this.ConnectionGroupUI.SetActive(false);
        this.SideGroupUI.SetActive(true);
        this.connInfo.text = info;
    }

    public void Disconnected(string info)
    {
        this.ConnectionGroupUI.SetActive(true);
        this.SideGroupUI.SetActive(false);
        Debug.Log(info);
        this.connInfo.text = "";
    }
}