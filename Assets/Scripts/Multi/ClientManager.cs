﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ClientManager.cs" company="VNG">
// 
// </copyright>
// <summary>
//   Defines the ClientManager.cs
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class ClientManager : MonoBehaviour
{
    private NetworkClient client;

    void Update()
    {

    }

    public void BecomeClient()
    {
        this.client = NetworkManager.singleton.StartClient();
        this.client.RegisterHandler(MsgType.Connect, OnConnected);
        this.client.RegisterHandler(MsgType.Disconnect, this.OnDisconnected);
    }

    public void BecomeHost()
    {
        this.client = NetworkManager.singleton.StartHost();
        this.client = NetworkManager.singleton.StartClient();
        this.client.RegisterHandler(MsgType.Connect, OnConnected);
        this.client.RegisterHandler(MsgType.Disconnect, this.OnDisconnected);
    }

    private void OnConnected(NetworkMessage msg)
    {
        UIManagerMulti.Instance.Connected(msg.ToString());
    }

    private void OnDisconnected(NetworkMessage msg)
    {
        UIManagerMulti.Instance.Disconnected(msg.ToString());
    }
}