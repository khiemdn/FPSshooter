﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class SpawnPlayerManager : NetworkBehaviour
{
    public GameObject canvas;

    void Start()
    {
        
    }

    [Command]
    public void CmdSpawnHuman()
    {
        NetworkClient client = NetworkManager.singleton.StartHost();
        Connecting(client);
        GameObject Human = Instantiate(NetworkManager.singleton.spawnPrefabs[0], NetworkManager.singleton.GetStartPosition().position, Quaternion.identity);
        NetworkServer.SpawnWithClientAuthority(Human, client.connection);
        Debug.Log(client.isConnected);
        this.canvas.SetActive(false);
    }

    [Command]
    public void CmdSpawnZombie()
    {
        NetworkClient client = NetworkManager.singleton.StartClient();
        client.Connect("127.0.0.1", 7777);
        Connecting(client);
        GameObject Zombie = Instantiate(NetworkManager.singleton.playerPrefab, NetworkManager.singleton.GetStartPosition().position, Quaternion.identity);
        NetworkServer.SpawnWithClientAuthority(Zombie, client.connection);
        this.canvas.SetActive(false);
    }

    IEnumerator Connecting(NetworkClient client)
    {
        while (!client.isConnected)
        {
            yield return null;
        }
    }
}
