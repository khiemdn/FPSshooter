﻿using UnityEngine;

public class Bullet : MonoBehaviour
{
    public GameObject explosionPrefab;

    private SphereCollider dmgTrigger;

    private BoxCollider explosionTrigger;

    // Use this for initialization
    private void Start()
    {
        Destroy(gameObject, 3);
        dmgTrigger = this.GetComponent<SphereCollider>();
        explosionTrigger = this.GetComponent<BoxCollider>();
        dmgTrigger.enabled = false;
    }

    // Update is called once per frame
    private void Update()
    {
    }

    private void OnCollisionEnter(Collision other)
    {
        if (this.explosionPrefab != null)
        {
            var exp = Instantiate(this.explosionPrefab, this.transform.position, other.transform.rotation);
            Destroy(exp, 3f);
        }
        explosionTrigger.enabled = false;
        this.dmgTrigger.enabled = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Enemies"))
        {
            IEnemies zombie = other.gameObject.GetComponent<IEnemies>();
            zombie.TakingDmg(40);
        }
        Destroy(this.gameObject, 1f);
    }
}