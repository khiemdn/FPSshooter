﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Gun2.cs" company="VNG">
//   FPS
// </copyright>
// <summary>
//   Defines the Gun2.cs
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Gun2
{
    using UnityEngine;

    /// <summary>
    /// The gun 2.
    /// </summary>
    public class Gun2 : MonoBehaviour
    {
        /// <summary>
        /// The bullet prefab.
        /// </summary>
        public GameObject bulletPrefab;

        /// <summary>
        /// The gun hole.
        /// </summary>
        private Transform gunHole;

        /// <summary>
        /// The animator.
        /// </summary>
        private Animator animator;

        /// <summary>
        /// The reload audio.
        /// </summary>
        private AudioSource reloadAudio;

        /// <summary>
        /// The max ammo.
        /// </summary>
        public const int MAX_AMMO = 1;

        /// <summary>
        /// The current ammo.
        /// </summary>
        [SerializeField]
        private int currentAmmo;
        
        /// <summary>
        /// Use this for initialization
        /// </summary>
        private void Start()
        {
            this.gunHole = this.transform.GetChild(0);
            this.animator = this.GetComponent<Animator>();
            this.reloadAudio = this.GetComponent<AudioSource>();
        }

        /// <summary>
        /// Update is called once per frame
        /// </summary>
        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                this.Shoot();
            }

            if (Input.GetKeyDown(KeyCode.R))
            {
                this.Reload();
            }
        }

        /// <summary>
        /// Event will be fired from Shoot clip
        /// </summary>
        private void Firing()
        {
            var bullet = Instantiate(this.bulletPrefab, this.gunHole.position, this.gunHole.transform.rotation)
                .GetComponent<Rigidbody>();
            bullet.AddForce(bullet.transform.forward * 5000f);
        }

        /// <summary>
        /// The reload.
        /// </summary>
        private void Reload()
        {
            if (!this.animator.GetBool("shooting") && !this.animator.GetBool("reloading"))
            {
                this.animator.SetTrigger("reload");
                this.reloadAudio.Play(0);
                this.animator.SetBool("canShoot", true);
                this.currentAmmo = MAX_AMMO;
                UIManager.Instance.UpdateBulletsBar((float)this.currentAmmo / MAX_AMMO);
            }
        }

        /// <summary>
        /// The shoot.
        /// </summary>
        private void Shoot()
        {
            if (!this.animator.GetBool("shooting") && !this.animator.GetBool("reloading"))
            {
                if (this.animator.GetBool("canShoot"))
                {
                    this.animator.SetTrigger("shoot");
                    this.animator.SetBool("canShoot", false);
                    this.currentAmmo--;
                    UIManager.Instance.UpdateBulletsBar((float)this.currentAmmo / MAX_AMMO);
                }
                else
                {
                    this.Reload();
                }
            }
        }
    }
}