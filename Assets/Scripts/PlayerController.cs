﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerController.cs" company="VNG">
// 
// </copyright>
// <summary>
//   Defines the PlayerController.cs
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
public class PlayerController : MonoBehaviour
{
#pragma warning disable CS0108 // Member hides inherited member; missing new keyword
    public Camera camera;
    
    private Animator animator;

    private Rigidbody body;

    public GameObject[] guns;

    private int currentGun;

    private const float MAX_HEALTH = 100;

    public float health = 100;

    private AudioSource footStepsAudio;

    // Use this for initialization
    private void Start()
    {
        this.body = this.GetComponent<Rigidbody>();
        this.animator = this.guns[this.currentGun].GetComponent<Animator>();
        this.footStepsAudio = this.GetComponent<AudioSource>();
    }

    // Update is called once per frame
    private void Update()
    {
        if (this.animator != null)
        {
            if (this.body.velocity.magnitude == 0)
            {
                if (this.footStepsAudio.isPlaying)
                {
                    this.footStepsAudio.Stop();
                }

                this.animator.SetBool("idling", true);
                this.animator.SetBool("walking", false);
            }
            else
            {
                if (!this.footStepsAudio.isPlaying)
                {
                    this.footStepsAudio.Play(0);
                }

                this.animator.SetBool("walking", true);
                this.animator.SetBool("idling", false);
            }
        }

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            var gun = this.SwitchGun(0);
            if (gun != null)
            {
                this.animator = gun.GetComponent<Animator>();
            }
        }

        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            var gun = this.SwitchGun(1);
            if (gun != null)
            {
                this.animator = gun.GetComponent<Animator>();
            }
        }
    }

    private void OnDrawGizmos()
    {
        if (this.camera != null)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawRay(this.camera.transform.position, this.camera.transform.forward * 10f);
        }
    }

    private GameObject SwitchGun(int index)
    {
        if (index < this.guns.Length && index != this.currentGun)
        {
            this.guns[this.currentGun].SetActive(false);
            this.guns[index].SetActive(true);
            this.currentGun = index;
            return this.guns[index];
        }

        return null;
    }

    public void TakingDmg(float dmg)
    {
        this.health -= dmg;
        UIManager.Instance.UpdateHealthBar(this.health / MAX_HEALTH);
        if (this.health <= 0)
        {
            Debug.Log("u die");
        }
    }
}