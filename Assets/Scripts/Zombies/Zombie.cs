﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Zombie.cs" company="VNG">
// FPS
// </copyright>
// <summary>
//   Defines the Zombie.cs
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Zombies
{
    using UnityEngine;
    using UnityEngine.AI;
    using UnityEngine.UI;

    /// <summary>
    /// Zombie class
    /// </summary>
    public class Zombie : MonoBehaviour, IEnemies
    {
        /// <summary>
        /// The ma x_ health.
        /// </summary>
        private const float MAX_HEALTH = 100;

        /// <summary>
        /// The health.
        /// </summary>
        public float health = 100;

        /// <summary>
        /// The animator.
        /// </summary>
        private Animator animator;

        /// <summary>
        /// The body.
        /// </summary>
        private Rigidbody body;

        /// <summary>
        /// The target.
        /// </summary>
        private Transform target;

        /// <summary>
        /// The player.
        /// </summary>
        private PlayerController player;

        /// <summary>
        /// The zombie collider.
        /// </summary>
        private CapsuleCollider zombieCollider;

        /// <summary>
        /// The speed.
        /// </summary>
        public float speed = 1.5f;

        /// <summary>
        /// The agent.
        /// </summary>
        private NavMeshAgent agent;

        /// <summary>
        /// The attack range.
        /// </summary>
        public float attackRange = 1.2f;

        /// <summary>
        /// The reach range.
        /// </summary>
        public float reachRange = 1.5f;

        /// <summary>
        /// The health bar.
        /// </summary>
        public Image healthBar;

        /// <summary>
        ///     Use this for initialization
        /// </summary>
        private void Start()
        {
            this.target = GameObject.FindGameObjectWithTag("Player").transform;
            this.zombieCollider = this.GetComponent<CapsuleCollider>();
            this.animator = this.GetComponent<Animator>();
            this.body = this.GetComponent<Rigidbody>();
            this.agent = this.GetComponent<NavMeshAgent>();
            this.player = this.target.GetComponent<PlayerController>();

            this.agent.speed = this.speed;
            this.animator.SetFloat("moveOffset", Random.Range(0f, 1f));
            this.DisableRagdoll();
        }


        /// <summary>
        /// Update is called once per frame
        /// </summary>
        private void Update()
        {
            if (this.health > 0)
            {
                var distance = (this.transform.position - this.target.position).magnitude;
                if (distance < this.attackRange)
                {
                    this.StopMoving();
                    this.Attack();
                }
                else
                {
                    this.MoveToTarget();
                }
            }
        }

        /// <summary>
        /// The taking dmg.
        /// </summary>
        /// <param name="dmg">
        /// The dmg.
        /// </param>
        public void TakingDmg(float dmg)
        {
            this.health -= dmg;
            this.healthBar.fillAmount = this.health / MAX_HEALTH;
            if (this.health <= 0)
            {
                this.EnableRagdoll();

                // this.StopMoving();
                // this.animator.SetTrigger("dead");
                // this.body.velocity = Vector3.zero;
                // Destroy(this.gameObject, 3f);
            }
        }

        /// <summary>
        /// The attack.
        /// </summary>
        public void Attack()
        {
            this.animator.SetTrigger("attack");
        }

        /// <summary>
        /// The stop moving.
        /// </summary>
        public void StopMoving()
        {
            this.agent.speed = 0;
            this.animator.SetBool("moving", false);
            this.body.velocity = Vector3.zero;
        }

        /// <summary>
        /// The move to target.
        /// </summary>
        public void MoveToTarget()
        {
            if (this.agent.enabled)
            {
                this.agent.speed = this.speed;
                this.agent.SetDestination(this.target.position);
                this.animator.SetBool("moving", true);
            }
        }

        /// <summary>
        /// The attack end.
        /// </summary>
        private void AttackEnd()
        {
            var distance = Vector3.Distance(this.transform.position, this.target.position);
            if (distance <= this.reachRange)
            {
                this.player.TakingDmg(10);
            }
        }

        /// <summary>
        /// The enable ragdoll.
        /// </summary>
        private void EnableRagdoll()
        {
            this.animator.enabled = false;
            this.agent.enabled = false;
            this.zombieCollider.enabled = false;
            var cols = this.GetComponentsInChildren<Collider>();
            foreach (Collider col in cols)
            {
                var ragdollBody = col.GetComponent<Rigidbody>();
                if (ragdollBody != null)
                {
                    ragdollBody.isKinematic = false;
                    ragdollBody.useGravity = true;
                }
            }

            // SkinnedMeshRenderer[] sk = GetComponentsInChildren<SkinnedMeshRenderer>();
            // for (int i = 0; i < sk.Length; i++)
            // {
            // if (sk[i].gameObject.name == "mesh_Head")
            // {
            // sk[i].bones[0].GetComponentInChildren<MeshRenderer>().enabled = true;
            // sk[i].bones[0].gameObject.GetComponent<CharacterJoint>().breakForce = 0;
            // sk[i].gameObject.SetActive(false);
            // }
            // }
        }

        /// <summary>
        ///     Disable ragdoll.
        /// </summary>
        private void DisableRagdoll()
        {
            var cols = this.GetComponentsInChildren<Collider>();
            foreach (Collider col in cols)
            {
                var ragdollBody = col.GetComponent<Rigidbody>();
                if (ragdollBody != null)
                {
                    ragdollBody.isKinematic = true;
                }
            }
        }
    }
}