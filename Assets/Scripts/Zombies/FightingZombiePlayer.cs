﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FightingZombiePlayer.cs" company="VNG">
// 
// </copyright>
// <summary>
//   Defines the FightingZombiePlayer.cs
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using UnityEngine;
using UnityEngine.Networking;

public class FightingZombiePlayer : NetworkBehaviour, IEnemies
{
    private Animator _animator;

    private NetworkAnimator _networkAnimator;

    private Rigidbody _body;

    private float _health = 200;

    private bool _isAttacking = false;

    // Use this for initialization
    private void Start()
    {
        this._animator = this.GetComponent<Animator>();
        this._networkAnimator = this.GetComponent<NetworkAnimator>();
        this._body = this.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    private void Update()
    {
        if (!this.isLocalPlayer) return;

        var h = Input.GetAxis("Horizontal");
        var v = Input.GetAxis("Vertical");

        if (h != 0 || v != 0)
        {
            Vector3 direction = v * Vector3.forward + h * Vector3.right;
            if (!_isAttacking)
            {
                Move(direction.normalized);
            }
        }
        else
        {
            StopMoving();
        }

        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            _isAttacking = true;
            this.StopMoving();
            this.Attack();
        }
    }

    private void Move(Vector3 direction)
    {
        Quaternion targetRotation = Quaternion.LookRotation(direction);
        transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, 360f * Time.deltaTime);
        this._body.velocity = transform.forward * 2f;
        this._animator.SetBool("moving", true);
    }

    private void StopMoving()
    {
        this._body.velocity = Vector3.zero;
        this._animator.SetBool("moving", false);
    }

    /// <summary>
    /// All after taking dmg animation
    /// </summary>
    private void Recovered()
    {
        this._animator.SetBool("takingDmg", false);
    }

    /// <summary>
    /// The attack.
    /// </summary>
    public void Attack()
    {
        this._animator.SetTrigger("attack");
        this._networkAnimator.SetTrigger("attack");
    }

    /// <summary>
    /// The taking dmg.
    /// </summary>
    /// <param name="dmg">
    /// The dmg.
    /// </param>
    public void TakingDmg(float dmg)
    {
        this._health -= dmg;
        this.StopMoving();
        this._animator.SetTrigger("hit");
        this._networkAnimator.SetTrigger("hit");
        this._animator.SetBool("takingDmg", true);
        if (this._health <= 0)
        {
            this._animator.SetTrigger("dead");
            this._networkAnimator.SetTrigger("dead");
            Debug.Log("U have been defeated");
        }
    }

    public void MoveToTarget()
    {
        // Only impl when this is an agent
    }

    public void AttackEnd()
    {
        _isAttacking = false; 
    }
}