﻿/// <summary>
/// The Enemies interface.
/// </summary>
public interface IEnemies
{
    /// <summary>
    /// The taking dmg.
    /// </summary>
    /// <param name="dmg">
    /// The dmg.
    /// </param>
    void TakingDmg(float dmg);

    /// <summary>
    /// The move to target.
    /// </summary>
    void MoveToTarget();
}