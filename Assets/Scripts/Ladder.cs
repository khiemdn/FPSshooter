﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityStandardAssets.Characters.FirstPerson;

public class Ladder : MonoBehaviour
{
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            RigidbodyFirstPersonController playerController = other.GetComponent<RigidbodyFirstPersonController>();
            playerController.StartClimbing(transform);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            RigidbodyFirstPersonController playerController = other.GetComponent<RigidbodyFirstPersonController>();
            playerController.EndClimbing(transform);
        }
    }
}
