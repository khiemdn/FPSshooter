﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Gun1.cs" company="VNG">
// FPS
// </copyright>
// <summary>
//   Defines the Gun1.cs
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Gun1
{
    using UnityEngine;

    /// <summary>
    /// Gun1
    /// </summary>
    public class Gun1 : MonoBehaviour
    {
        /// <summary>
        /// The camera.
        /// </summary>
        public new Camera camera;

        /// <summary>
        /// The bullet trail.
        /// </summary>
        public ParticleSystem bulletTrail;

        /// <summary>
        /// The animator.
        /// </summary>
        private Animator animator;

        /// <summary>
        /// The reload audio.
        /// </summary>
        private AudioSource reloadAudio;

        /// <summary>
        /// The out of ammo audio.
        /// </summary>
        private AudioSource outOfAmmoAudio;

        /// <summary>
        /// The shoot audio.
        /// </summary>
        private AudioSource shootAudio;

        /// <summary>
        /// The dirt impact.
        /// </summary>
        public GameObject dirtImpact;

        /// <summary>
        /// The sparks.
        /// </summary>
        public GameObject sparks;

        /// <summary>
        /// The blood.
        /// </summary>
        public GameObject blood;

        public const int MAX_AMMO = 30;

        /// <summary>
        /// The damage.
        /// </summary>
        public float damage = 20;

        /// <summary>
        /// The current ammo.
        /// </summary>
        public int currentAmmo = 30;

        /// <summary>
        /// The bullet hole.
        /// </summary>
        public GameObject bulletHole;
        
        /// <summary>
        /// Use this for initialization
        /// </summary>
        private void Start()
        {
            this.animator = this.GetComponent<Animator>();
            this.reloadAudio = this.GetComponents<AudioSource>()[0];
            this.outOfAmmoAudio = this.GetComponents<AudioSource>()[1];
            this.shootAudio = this.GetComponents<AudioSource>()[2];
        }
        
        /// <summary>
        /// Update is called once per frame
        /// </summary>
        private void Update()
        {
            if (Input.GetKey(KeyCode.Mouse0))
            {
                this.Shoot();
            }

            if (Input.GetKeyUp(KeyCode.Mouse0))
            {
                this.StopShooting();
            }

            if (Input.GetKeyDown(KeyCode.R))
            {
                this.Reload();
            }
        }

        /// <summary>
        /// Start firing.
        /// </summary>
        private void Firing()
        {
            if (this.currentAmmo <= 0)
            {
                this.outOfAmmoAudio.Play(0);
                return;
            }

            this.shootAudio.Play(0);
            this.currentAmmo--;
            UIManager.Instance.UpdateBulletsBar((float)this.currentAmmo / MAX_AMMO);
            var ray = this.camera.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0f));
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 500f, ~LayerMask.GetMask("Ally")))
            {
                if (hit.collider.CompareTag("Enemies"))
                {
                    var e = hit.transform.gameObject.GetComponent<IEnemies>();
                    e.TakingDmg(this.damage);
                    var zombieBlood = Instantiate(
                        this.blood,
                        hit.point,
                        Quaternion.LookRotation(this.camera.transform.position - hit.point));
                    Destroy(zombieBlood, 1f);
                }
                else
                {
                    var dirt = Instantiate(this.dirtImpact, hit.point, Quaternion.LookRotation(hit.normal));
                    Destroy(dirt, 2f);
                    var hole = Instantiate(
                        this.bulletHole,
                        hit.point + hit.normal * 0.01f,
                        Quaternion.LookRotation(hit.normal),
                        hit.collider.transform);
                    Destroy(hole, 3f);
                }

                var main = this.bulletTrail.main;
                var lifetime = main.startLifetime;
                main.startLifetime = Vector3.Distance(this.bulletTrail.transform.position, hit.point)
                                     / main.startSpeed.constant;

                // this.bulletTrail.transform.LookAt(hit.point);
                this.bulletTrail.Emit(1);
            }
            else
            {
                var main = this.bulletTrail.main;
                var lifetime = main.startLifetime;
                main.startLifetime = 1f;
                this.bulletTrail.Emit(1);
            }
        }

        private void Reload()
        {
            if (!this.animator.GetBool("shooting") && !this.animator.GetBool("reloading"))
            {
                this.animator.SetTrigger("reload");
                this.reloadAudio.Play(0);
                this.animator.SetBool("canShoot", true);
                this.currentAmmo = MAX_AMMO;
                UIManager.Instance.UpdateBulletsBar(1);
            }
        }

        /// <summary>
        /// Begin shooting
        /// </summary>
        private void Shoot()
        {
            if (!this.animator.GetBool("shooting") && !this.animator.GetBool("reloading"))
            {
                if (this.animator.GetBool("canShoot"))
                {
                    this.animator.SetTrigger("shoot");
                    this.animator.SetBool("shooting", true);
                }
            }
        }

        private void StopShooting()
        {
            this.animator.SetBool("shooting", false);
        }
    }
}