﻿using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public static UIManager Instance
    {
        get
        {
            return _instance;
        }
    }

    private static UIManager _instance = null;

    public Image healthBar;

    public Image bulletsBar;

    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        } else if (_instance != this)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(this);
    }

    public void UpdateHealthBar(float ratio)
    {
        this.healthBar.fillAmount = ratio;
    }

    public void UpdateBulletsBar(float ratio)
    {
        bulletsBar.fillAmount = ratio;
    }
}